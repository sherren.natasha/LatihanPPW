from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.

class HomePageView(TemplateView):
    template_name = "Home.html"

class ProfilePageView(TemplateView):
    template_name = "Profile.html"
	
class ContactPageView(TemplateView):
    template_name = "Contact.html"
	
class WorksPageView(TemplateView):
    template_name = "MyWorks.html"

class RegisterPageView(TemplateView):
    template_name = "Register.html"