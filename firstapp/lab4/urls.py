from django.conf.urls import url
from lab4 import views


urlpatterns = [
    url(r'^$', views.HomePageView.as_view()),
    url(r'^Home/$', views.HomePageView.as_view()),
	url(r'^Profile/$',views.ProfilePageView.as_view()),
	url(r'^Contact/$',views.ContactPageView.as_view()),
	url(r'^MyWorks/$',views.WorksPageView.as_view()),
	url(r'^Register/$', views.RegisterPageView.as_view()),

]